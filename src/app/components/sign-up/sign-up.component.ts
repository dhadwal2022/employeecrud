import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from 'src/app/model/employee';
import { SignUpUser } from './signup';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

 user:SignUpUser
 courseList:Array<any>
 signup: NgForm | undefined;
  constructor() {
   this.user = new SignUpUser();
   this.courseList=['Full Stack Development(Java)',
   'Full Stack Development(.Net)','Full Development in Big Data']
   
   }

  ngOnInit(): void {
    
  }
  signupData(val:NgForm){
    console.log(val.value)
    console.log('this is button');
    val.reset()
  }

}
