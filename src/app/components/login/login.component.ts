
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup
  // formGroupDirective: FormGroupDirective
  constructor( private formBuilder:FormBuilder , private location:Location) {
    this.loginForm = this.formBuilder.group({
     email:['',Validators.compose([Validators.required,
      Validators.minLength(4)])],
     password:['',Validators.compose([Validators.required,
      Validators.minLength(5)])]
    })
   }
 
  ngOnInit(): void {
    
  }
  validateUser(val: FormGroup){
    console.log(val.value)
    // this.location.forward()
    // this.location.go('dashboard') 
    this.location.back()

  }

}
