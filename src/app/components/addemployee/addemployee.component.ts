import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-addemployee',
  templateUrl: './addemployee.component.html',
  styleUrls: ['./addemployee.component.css']
})
export class AddemployeeComponent implements OnInit {

  employee:Employee
  employeeList:Array<any>;
  constructor(private employeeService:EmployeeService) {
    this.employee=new Employee()
    this.employeeList=[]
   }
  
  ngOnInit(): void {
  }
  
  saveEmployee(){
    // console.log(this.employee.name)
    // console.log(this.employee)
    this.employeeService.saveData(this.employee).subscribe(newemp =>{
       console.log('Data Saved',newemp);
       this.employeeList.push(newemp)
       this.employee = new Employee();
    })
  }
}
