import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  employee: Employee
  id:any
  constructor(private employeeService:EmployeeService,
     private route:ActivatedRoute,
    private router:Router) { 
    this.employee = new Employee()
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
             this.id =params['id']
    })
    console.log(this.id)

    this.employeeService.getById(this.id).subscribe(resp =>{
      this.employee = resp
      console.log(this.employee)
    })
  }


  updateData(val:any){
    console.log(val)
    this.employeeService.updateData(val).subscribe(resp =>{
      alert('Data Updated')
    })
  }
}
