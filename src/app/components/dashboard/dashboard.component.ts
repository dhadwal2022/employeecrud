import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  employee: Employee;  
  employeeList:Array<any>;
  errorMessage :string ='';
  constructor(private employeeService: EmployeeService) {
    this.employee = new Employee();
    this.employeeList = []
   }
 

  ngOnInit(): void {
    this.employeeService.fetchEmployeeFromServer().subscribe(resp =>{
      this.employeeList = resp
    },error =>{
      console.log(error)
      this.errorMessage = error.message
    })
  }

  // saveEmployee(){
  //   // console.log(this.employee.name)
  //   // console.log(this.employee)
  //   this.employeeService.saveData(this.employee).subscribe(newemp =>{
  //      console.log('Data Saved',newemp);
  //      this.employeeList.push(newemp)
  //      this.employee = new Employee();
  //   })
  // }


  deleteRecord(id:any){
   console.log(id)
   this.employeeService.deleteEmployee(id).subscribe(resp =>{
    alert('data Deleted')
    this.ngOnInit()
   })
  }

}
