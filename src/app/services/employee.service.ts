import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  employee: Employee
  constructor(private http: HttpClient) {
    this.employee = new Employee()    
   }
  //  brearer ='abc123'
  //    headers = new HttpHeaders()
  //    .set('content-type','application/json')
  //    .set('Access-Control-Allow-Origin','*')
  //    .set('Authorization',this.brearer)


   fetchEmployeeFromServer():Observable<any>{
     return this.http.get('http://localhost:3000/employee')
    // return this.http.get('https://jsonplaceholder.typicode.com/posts')
   }

   saveData(employee: Employee ):Observable<Employee>{
    return this.http.post<Employee>('http://localhost:3000/employee',employee)
   }

   getById(val:any):Observable<any>{
   return this.http.get(`http://localhost:3000/employee/${val}`)
   }

   updateData(val:any):Observable<any>{
    return this.http.put(`http://localhost:3000/employee/${val.id}`,val)
   }

   deleteEmployee(val:any):Observable<any>{
    return this.http.delete(`http://localhost:3000/employee/${val}`)
   }
}
